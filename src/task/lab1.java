package task;

public class lab1 {

//avg
    public double begin8(int a, int b) {
        return (a + b) / 2.;
    }

//change number
    public int integer8(int num) {
        int a = num / 10, b = num % 10;
        return b * 10 + a;
    }

//both odd
    public boolean boolean8(int a, int b) {
        return (a % 2 == 1) & (b % 2 == 1);
    }

//max,min
    public String if8(int a, int b) {
        String str;
        if (a >= b)
            str = a + ", " + b;
        else
            str = b + ", " + a;
        return str;
    }

// prew date
    public String case8(int d, int m) {
        switch (m) {
            case 5:
            case 7:
            case 10:
            case 12:
                if (d == 1) {
                    d = 30;
                    m -= 1;
                } else {
                    d -= 1;
                }
                break;
            case 1:
            case 2:
            case 4:
            case 6:
            case 8:
            case 9:
            case 11:
                if (d == 1) {
                    d = 31;
                    m -= 1;
                } else {
                    d -= 1;
                }
                break;
            case 3:
                if (d == 1) {
                    d = 28;
                    m -= 1;
                } else {
                    d -= 1;
                }
                break;
            default:
                return "Month is not regular";
        }
        return d + "." + m;
    }

//mult from a to b 1,2 => 2; -3, 4 => 0
    public int for8(int a, int b) {
        int m = 1;
        for (int i = a; i <= b; ++i)
            m *= i;
        return m;
    }

//find sqr(k)<=n
    public int while8(int n) {
        int i = 0;
        while (i * i <= n) {
            i++;
        }
        return i-1;
    }

//koef of progr
    public double array25(int[] mas) {
        int n = mas.length;
        for (int i = 0; i < n - 2; ++i) {
            if (mas[i + 1] / mas[i] != mas[i + 2] / mas[i + 1])
                return 0;
        }
        return (double)mas[1] / mas[0];
    }

//change first<0 with n
    public int[][] matrix54(int[][] arr, int n, int m) {
        for (int j = 0; j < n; ++j) {
            int k = 0;
            for (int i = 0; i < m; ++i) {
                if (arr[i][j] < 0) k++;
            }
            if (k == m) {
                for (int i = 0; i < m; i++) {
                    int buf = arr[i][j];
                    arr[i][j] = arr[i][n - 1];
                    arr[i][n - 1] = buf;
                }
                return arr;
            }
        }
        return arr;
    }
}