package test;


import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import task.lab1;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class testForLab1 {
    @Test
    public void testForBegin8(){
        double actual = new lab1().begin8(2,8);
        double expected = 5.0;
        assertEquals(actual,expected);
    }

    @Test
    public void testForInteger8(){
        assertEquals(new lab1().integer8(34),43);
        assertEquals(new lab1().integer8(89),98);
    }

    @Test
    public void testForBoolean8(){
        assertFalse(new lab1().boolean8(4,9));
        assertTrue(new lab1().boolean8(5,3));
    }

    @Test
    public void testForIf8(){
        assertEquals(new lab1().if8(18,5),"18, 5");
        assertEquals(new lab1().if8(3,9),"9, 3");
    }

    @Test
    public void testForCase8(){
        assertEquals(new lab1().case8(1,9),"31.8");
        assertEquals(new lab1().case8(3,6),"2.6");
    }

    @Test
    public void testForFor8(){
        assertEquals(new lab1().for8(4,6), 120);
        assertEquals(new lab1().for8(-3,4),0);
    }

    @Test
    public void testForWhile8(){
        assertEquals(new lab1().while8(10),3);
        assertEquals(new lab1().while8(25),5);
    }

    @Test(dataProvider = "arrayProvider")
    public void arrayTest(int[] array, double value) {
        assertEquals(new lab1().array25(array), value);
    }

    @DataProvider
    public Object[][] arrayProvider() {
        return new Object[][]{{new int[]{8,4,2,1}, 0.5}, {new int[]{10,20,30,40}, 0}};
    }

    @Test(dataProvider = "matrixProvider")
    public void twoDimensionArrayTest(int[][] input, int n, int m, int[][] output) {
        assertEquals(new lab1().matrix54(input, n, m), output);
    }

    @DataProvider
    public Object[][] matrixProvider() {
        int[][] input = {{-2, 3, 6, 9, -9},
                {-34, 98, -9, 2, 1},
                {-4, 2, 1, 6, 1},
                {-98, 8, 1, 5, 3}};

        int[][] input1 = {{-9, 3, 6, 9, -2},
                {1, 98, -9, 2, -34},
                {1, 2, 1, 6, -4},
                {3, 8, 1, 5, -98}};

        int[][] input2 = {{-98, 8, 1, 5, 3},
                {-4, 2, 1, 6, 1},
                {34, 98, -9, 2, 1},
                {2, 3, 6, 9, -9}};

        return new Object[][]{{input, 5, 4, input1}, {input2, 5, 4, input2}};
    }
}
